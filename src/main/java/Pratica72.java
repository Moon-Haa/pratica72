
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.pratica.ContadorPalavras;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Moon-Haa
 */
public class Pratica72 {
   public static void main(String[] args) throws FileNotFoundException, IOException {
       
       System.out.println("Digite o nome e o caminho completo de arquivo de texto:");
       
       Scanner input = new Scanner(System.in);
       File file_in = new File(input.next());
       
       if(!file_in.canRead()){
           System.out.println("Erro na leitura do arquivo de entrada!");
           System.exit(0);
       }
       
       ContadorPalavras wordCount = new ContadorPalavras(file_in);
       Map<String,Integer> map = new HashMap<>(wordCount.getPalavras());
       LinkedHashMap<String,Integer> map_ordered = new LinkedHashMap<>();
       
       order(map,map_ordered);
       
       File file_out = new File(file_in+".out");
       
       if(!file_out.createNewFile()){
           System.out.println("Erro na criacao do arquivo de saida!");
           System.exit(0);
       }
       
       BufferedWriter writter = new BufferedWriter(new FileWriter(file_out));
       
       for ( Map.Entry<String, Integer> entry : map_ordered.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            
            writter.write(key+","+value.toString());
            writter.newLine();
       }
       
       writter.close();
       
       System.out.println("Programa executado com sucesso!");
    }
   
   public static void order(Map<String,Integer> map_in,LinkedHashMap<String,Integer> map_out){
       //ordernar pelo value (Integer)
       Integer compare = 0; //nunca vai ter uma ocorrencia 0
       String word = null;
       
       for ( Map.Entry<String, Integer> entry : map_in.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            
            if(value > compare) {
                compare = value;
                word = key;
            }
        }
       
       map_out.put(word, compare);
       map_in.remove(word, compare);
       
       if(map_in.size()>0)
        order(map_in,map_out);
   }
}
