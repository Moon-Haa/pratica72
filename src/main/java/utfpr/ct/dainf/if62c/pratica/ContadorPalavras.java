/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Moon-Haa
 */
public class ContadorPalavras {

    private BufferedReader reader;
    
    public ContadorPalavras(File root) throws FileNotFoundException {
        reader = new BufferedReader(new FileReader(root));
    }
    
    public Map<String, Integer> getPalavras() throws IOException {
        
        Map<String, Integer> map = new HashMap<String,Integer>();
        String inputLine = null;
        
        while ((inputLine = reader.readLine()) != null) {
            String[] words = inputLine.split("[ \n\t\r.,;:!?(){}]");

            for (String key : words) {
                if (key.length() > 0) {
                    if (map.get(key) == null) {
                        map.put(key, 1);
                    }
                    else {
                        int value = map.get(key).intValue();
                        value++;
                        map.put(key, value);
                    }
                }
            }
        }
 
        reader.close();
        
        return map;
    }
}
